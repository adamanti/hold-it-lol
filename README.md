# hold-it-lol
Quality of life [https://objection.lol/courtroom](objection.lol) courtroom features & tweaks, ranging from tiny UI changes to roleplay testimony UIs.

**Important:** the extension is still in beta - some assets are unfinished.

## Options

Upon installation, there is a page of options with descriptions to personalize your courtroom experience and pick only the features you like.

It can be accessed by clicking the extension icon in the top right (you may need to pin it in the extensions list).

<details>
<summary><i>A full list of features provided by the extension</i></summary>
<blockquote>

<details><summary>Convenience</summary><br><blockquote><table>
<tr><td>Auto-recording</td><td>Automatically start recording joined courtrooms (saving is manual).</td></tr>
<tr><td>Remember last character</td><td>The last character you used (with the extension on) is selected by default in the next court.</td></tr>
<tr><td>Unblur pixel characters</td><td>Makes character poses with low resolutions sharp instead of blurry.</td></tr>
<tr><td>Disable T key</td><td>Turn off the "T" hotkey that toggles "give testimony".</td></tr>
<tr><td>"Now playing..." display</td><td>Shows the name given to the currently playing track.</td></tr>
<tr><td>Auto-closing menus</td><td>Automatically close formatting menus after you've used them.</td></tr>
<tr><td>Open menus by hovering</td><td>Open formatting menus by hovering over them instead of clicking.</td></tr>
<tr><td>Quick sounds and music</td><td>Add sounds just by clicking on them in the list (without pressing "insert tag")<br>(Hold "SHIFT" to suppress)</td></tr>
</table></blockquote><br></details>
<details><summary>Messages</summary><br><blockquote><table>
<tr><td>New lines</td><td>Shift+Enter adds a new line.</td></tr>
<tr><td>Fix tags inside color tags</td><td>Fixes tags inside of color tags such as [#/r][#bgs1][/#] not working.</td></tr>
<tr><td>More color tags</td><td>Converts [#/y], [#/w] and [#/dr] into valid color tags.</td></tr>
<tr><td>"No talking" toggle</td><td>Disables your character's talking animation, just like in Objection Maker.</td></tr>
<tr><td>"Don't Delay Dialogue" toggle</td><td>Adds the "Don't Delay Dialogue" toggle from Objection Maker.</td></tr>
<tr><td>Quickly typing pauses</td><td>Press , again after a , or other punctuation marks to add pauses.<br>(Typing more , increases the delay.)</td></tr>
<tr><td>Effect hotkeys</td><td>Quickly add the Flash and Shake tags by pressing CTRL + 1, CTRL + 2, or CTRL + 3.</td></tr>
<tr><td>Color hotkeys</td><td>Quickly color selected text red, blue or green by pressing ALT + 1, ALT + 2, or ALT + 3.</td></tr>
<tr><td>Dual effect button</td><td>Insert both Flash and Shake at the same time.</td></tr>
<tr><td>Smart "to normal" poses</td><td>When switching poses, automatically plays the last pose's "to normal" if available.<br>(Lags less without Preload Resources.)</td></tr>
</table></blockquote><br></details>
<details><summary>Interface</summary><br><blockquote><table>
<tr><td>Classic toggles</td><td>Toggles like "Pre-animate" are accessible outside of a menu (as it was in the past).</td></tr>
<tr><td>Classic speech bubbles</td><td>Speech bubbles are selected from a column of buttons instead of a dropdown (as it was in the past).</td></tr>
<tr><td>Clickable chat links</td><td>URLs in chat messages become clickable. You can <i>also</i> right click to quickly save sounds & music.</td></tr>
<tr><td>Separate volume sliders</td><td>Adjust the volume of music and sound effects separately.</td></tr>
<tr><td>Fullscreen in court record</td><td>Mention full-screen evidence from the court record.</td></tr>
<tr><td>"Preload Resources" while spectating</td><td>Toggle "Preload Resources" while spectating.</td></tr>
<tr><td>"Reload custom characters"</td><td>Reload others' custom characters from Settings to see their changes without reloading the page.</td></tr>
</table></blockquote><br></details>
<details><summary>Moderation</summary><br><blockquote><table>
<tr><td>Automatic re-mute</td><td>(Discord auth required) Automatically re-mutes a muted user if they rejoin.</td></tr>
<tr><td>Moderate from chat log</td><td>Quickly mute or ban using buttons next to their messages.</td></tr>
<tr><td>Moderate from user list</td><td>Quickly mute, ban anyone or make them a moderator from the user list.</td></tr>
<tr><td>Hide character</td><td>Someone's character is laggy or unpleasant? Mute just the character, while still seeing their messages.</td></tr>
</table></blockquote><br></details>
<details><summary>New features</summary><br><blockquote><table>
<tr><td>Roleplay testimony</td><td>A witness testimony player for roleplay cases.</td></tr>
<tr><td>Add evidence from table</td><td>Automatically add lots of evidence via a copy-pasted table from a document.<br>(Works with tables where each evidence takes up a row)</td></tr>
<tr><td>Extended Log</td><td>A plainer, but longer and more detailed chat log mode.<br>(Can be useful to stenograph roleplay cases)</td></tr>
<tr><td>Quick sound effects</td><td>Save and quickly insert sounds from a row of sound effects.</td></tr>
<tr><td>Text-to-speech</td><td>Everyone speaks in text-to-speech voices lol</td></tr>
<tr><td>Pose icon maker</td><td>Add pose icons to characters that lack them using an in-courtroom editor.</td></tr>
<tr><td>Custom character archiver</td><td>Download and preserve character images.<br>(Warning: downloads will lag and may disconnect your current courtroom page)</td></tr>
</table></blockquote><br></details>
<details><summary>Music Packs</summary><br><blockquote><table>
<tr><td>Phoenix Wright: Ace attorney</td><td>Use Ace Attorney songs from any game in objection.lol beyond the default music.</td></tr>
<tr><td>Justice for All</td><td></td></tr>
<tr><td>Trials and Tribulations</td><td></td></tr>
<tr><td>Apollo Justice: Ace attorney</td><td></td></tr>
<tr><td>Dual Destinies</td><td></td></tr>
<tr><td>Spirit of Justice</td><td></td></tr>
<tr><td>The Great Ace Attorney: Adventures</td><td></td></tr>
<tr><td>The Great Ace Attorney: Resolve</td><td></td></tr>
<tr><td>Ace Attorney Investigations: Miles Edgeworth</td><td></td></tr>
<tr><td>Ace Attorney Investigations 2: Prosecutor's Path</td><td></td></tr>
<tr><td>Professor Layton vs. Phoenix Wright: Ace Attorney</td><td></td></tr>
</table></blockquote><br></details>

</blockquote>
</details>

## Discussion

If you have any questions, problems or suggestions regarding the extension, you can join our [Discord server](https://discord.gg/KqjQUrHuXH).

## Installation instructions

1. Download the top .zip file from the [latest release on Codeberg](https://codeberg.org/adamanti/hold-it-lol/releases/tag/v0.7.5).
1. Unzip the file and you should have a folder named plainly `hold-it-lol`.
1. In your Chrome-based browser go to the extensions page (for Chrome or Edge, `chrome://extensions` or `edge://extensions`).
1. Enable Developer Mode in the top right.
1. Drag the `hold-it-lol` folder onto the page to load it (do not delete the folder afterwards).

## Known Issues

Known bug warnings (as of v0.7.5 beta):

- Due to the way the extension loads, spectating breaks some UI elements; you'll be prompted to reload and join without spectating.
